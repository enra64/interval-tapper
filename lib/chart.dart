/// Example of a stacked area chart.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class StackedAreaLineChart extends StatelessWidget {
  final List<charts.Series<dynamic, num>> seriesList;

  StackedAreaLineChart(this.seriesList);

  @override
  Widget build(BuildContext context) {
    return new charts.LineChart(
      seriesList,
      defaultRenderer:
          new charts.LineRendererConfig(includeArea: true, stacked: true),
      primaryMeasureAxis: new charts.NumericAxisSpec(
          tickProviderSpec: new charts.BasicNumericTickProviderSpec(
              zeroBound: true,
              desiredTickCount: 20,
              dataIsInWholeNumbers: false),
          tickFormatterSpec:
              new charts.BasicNumericTickFormatterSpec.fromNumberFormat(
                  new NumberFormat("#0.00"))),
      animate: false,
    );
  }
}

class ChartItem {
  final int xPos;
  final DateTime endTime;
  final double interval;
  final double dividedInterval;

  ChartItem(this.xPos, this.endTime, this.interval, this.dividedInterval);

  Map toJson() {
    return {
      "endTime": endTime.toIso8601String(),
      "interval": interval,
      "modifiedInterval": dividedInterval,
    };
  }
}
