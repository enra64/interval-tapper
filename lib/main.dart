import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:interval_tapper/chart.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:share_plus/share_plus.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Interval Tapper',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(key: Key("key?"), title: 'Interval Tapper'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({required Key key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var timestampList = [];
  num divisor = 0.85;
  final _formKey = GlobalKey<FormState>();

  void _appendTimestamp() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      timestampList.add(new DateTime.now());
    });
  }

  String? _numberValidator(String? value) {
    if (value == null) {
      return null;
    }
    final n = num.tryParse(value);
    if (n == null) {
      return "Not a number";
    } else if (n <= 0) {
      return "Not above 0";
    }
    return null;
  }

  void _share() {
    var items = _getChartItems();
    var text = jsonEncode({
      "divisor": divisor,
      "times": items,
      "medianInfo": _median(),
    });
    Share.share(text, subject: "Interval Tapper Result");
  }

  void _showSettings() {
    var modController = TextEditingController();
    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Settings'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Select the divisor to calculate the modified interval, shown in green. '),
                Text(
                    'For example, the default 0.85 will show you what interval should be chosen to'
                    ' tap with 85% of your given (tapped) speed.'),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: "Enter divisor above 0",
                          hintText: divisor.toString(),
                        ),
                        keyboardType: TextInputType.number,
                        // Only numbers can be entered
                        validator: _numberValidator,
                        controller: modController,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Apply'),
              onPressed: () {
                if (_formKey.currentState?.validate() == true) {
                  Navigator.of(context).pop();
                  setState(() {
                    divisor = num.tryParse(modController.text) ?? 0.85;
                  });
                }
              },
            ),
          ],
        );
      },
    );
  }

  void _reset() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      timestampList.clear();
    });
  }

  List<ChartItem> _getChartItems() {
    List<ChartItem> data = [];
    for (var i = 1; i < timestampList.length; i++) {
      var diff = timestampList[i].difference(timestampList[i - 1]);
      var interval = diff.inMilliseconds / 1000;
      var modifiedInterval = interval / divisor;
      data.add(ChartItem(i - 1, timestampList[i], interval, modifiedInterval));
    }
    return data;
  }

  List<charts.Series<ChartItem, int>> _getSeries() {
    var chartItems = _getChartItems();

    return [
      new charts.Series<ChartItem, int>(
        id: 'Tap Intervals Full',
        colorFn: (_, __) => charts.MaterialPalette.gray.shadeDefault,
        domainFn: (ChartItem ci, _) => ci.xPos,
        measureFn: (ChartItem ci, _) => ci.interval,
        data: chartItems,
      ),
      new charts.Series<ChartItem, int>(
        id: 'Tap Intervals',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        domainFn: (ChartItem ci, _) => ci.xPos,
        measureFn: (ChartItem ci, _) => ci.dividedInterval - ci.interval,
        data: chartItems,
      )
    ];
  }

  double median(List<double> a) {
    var middle = a.length ~/ 2;
    if (a.length % 2 == 1) {
      return a[middle];
    } else {
      return (a[middle - 1] + a[middle]) / 2.0;
    }
  }

  String _median() {
    var items = _getChartItems();
    if (items.length > 0) {
      var unmodifiedMedian = median(items.map((i) => i.interval).toList());
      var dividedMedian = median(items.map((i) => i.dividedInterval).toList());
      var fUM = new NumberFormat("#0.00").format(unmodifiedMedian);
      var fDM = new NumberFormat("#0.00").format(dividedMedian);
      return "Unmodified median: $fUM; divided median: $fDM";
    } else {
      return "Medians are NaN!";
    }
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: const Icon(Icons.share),
            tooltip: 'Share JSON Result',
            onPressed: _share,
          ),
          IconButton(
            icon: const Icon(Icons.settings),
            tooltip: 'Settings',
            onPressed: _showSettings,
          ),
          IconButton(
            icon: const Icon(Icons.info_outline),
            tooltip: 'About',
            onPressed: () => showAboutDialog(
                context: context,
                applicationName: "Interval Tapper",
                applicationIcon: FlutterLogo(),
                applicationVersion: "2.0.3",
                children: [
                  Text("Made with ❤️ in about 5 hours."),
                  Text("Open source @ gitlab.com/enra64/interval-tapper")
                ]),
          ),
        ],
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Flexible(
              flex: 12,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                child: StackedAreaLineChart(_getSeries()),
              ),
            ),
            Flexible(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Text(_median())],
              ),
            ),
            Spacer(flex: 2),
            Flexible(
              flex: 4,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 0, 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: _reset,
                            child: Image.asset(
                              "assets/restart.png",
                              width: 56,
                              height: 56,
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                      ],
                    ),
                    ConstrainedBox(
                      constraints:
                          BoxConstraints.tightFor(width: 170, height: 170),
                      child: ElevatedButton(
                        child: Text(''),
                        onPressed: _appendTimestamp,
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
